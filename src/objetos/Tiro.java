
package objetos;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import slick.Game;


public class Tiro {
    private final String image_path = "imagens/tiro.png";
    private double x;
    private double y;
    private final Image image;
    private int angulo;
    private final int velocidade = 1; 
    
    
    
    public Tiro(GameContainer gc, int inclinacao, double x, double y) throws SlickException{
       
        this.angulo = inclinacao;
        
        this.image = new Image(image_path);
        this.x = (int)x;
        this.y = (int)y;
        this.image.setCenterOfRotation(this.image.getWidth()/2,this.image.getHeight()/2);
        image.setRotation((float)Math.toRadians(angulo));
        
        
    }
    
    public void render(GameContainer gc, Graphics g){
        
        
        g.drawImage(this.image, (int)this.x, (int)this.y);

    }
    
    public void update(){
        this.move();

    }
    
    private void move(){
        System.out.println();
       this.y -= velocidade * Math.cos(Math.toRadians(angulo));
       this.x += velocidade * Math.sin(Math.toRadians(angulo));
    }
    public void finalize (){
        
    }
    public double get_Y(){
        return y;
    }

    public boolean fora_da_tela() {
        if ((this.x > Game.width) || (this.x < 0))
            return true;
        if ((this.y > Game.height) || (this.y < 0))
            return true;
        return false;
    }
}

