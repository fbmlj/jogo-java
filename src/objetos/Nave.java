
package objetos;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import slick.Game;
import slick.Play;

public class Nave {
    private final String image_path = "imagens/nave.png";
    protected double x;
    protected double y;
    protected final Image image;
    private int angulo;
    private long tempo_para_atirar;
    private long tempo_para_virar_esq;
    private long tempo_para_virar_dir;
    public ListadeTiros tiros;
    private  int velocidade = 4000;
    
    public Nave(GameContainer gc) throws SlickException{
        this.angulo = 0;
        this.tempo_para_virar_esq = 0;
        this.tempo_para_virar_dir = 0;
        this.tiros = new ListadeTiros();
        this.tempo_para_atirar = 0;
        this.image = new Image(image_path);
        this.x = (Game.width/2) - (this.image.getWidth()/2);
        this.y = Game.height/2 - this.image.getHeight()/2;
        this.image.setCenterOfRotation(this.image.getWidth()/2,this.image.getHeight()/2);
        
        
    }
    
    public void render(GameContainer gc, Graphics g){
        
        image.setRotation((angulo));
        g.drawImage(this.image, Math.round(this.x), Math.round(y));

    }
    
    public void update(Input input, GameContainer gc) throws SlickException{
        if (input.isKeyDown(Input.KEY_A) && tempo_para_virar_esq>3){
            this.angulo -= 1;
            this.tempo_para_virar_esq = 0;
            
        }
        if (input.isKeyDown(Input.KEY_D) && tempo_para_virar_dir>3){
            this.angulo +=1;
            this.tempo_para_virar_dir = 0;
            
            
        }
        if (input.isKeyDown(Input.KEY_SPACE) && this.tempo_para_atirar > 200){
            this.tiros.novo_tiro(gc, this.angulo, this);
            this.tempo_para_atirar = 0;
        } 
        
        if (this.tempo_para_atirar < 201) this.tempo_para_atirar += Play.get_delta_time();
        
        if (this.tempo_para_virar_esq < 4) this.tempo_para_virar_esq += Play.get_delta_time();
        
        if (this.tempo_para_virar_dir < 4) this.tempo_para_virar_dir += Play.get_delta_time();
        
        if (input.isKeyDown(Input.KEY_W)) this.move();
        
    }

   private void move(){
//       System.out.println(angulo);
       this.y -= this.velocidade * Play.get_delta_time_seconds() * Math.cos(Math.toRadians(this.angulo));
       this.x += this.velocidade * Play.get_delta_time_seconds() * Math.sin(Math.toRadians(this.angulo));
       this.fora_da_tela();
       
   }
   
   private void fora_da_tela(){
       int width  = this.image.getWidth();
       int height  = this.image.getHeight();
       if (this.x > Game.width)   this.x = -width;
       if (this.x + width < 0) this.x = Game.width;
       if (this.y > Game.height)   this.y = -height;
       if (this.y + height < 0) this.y = Game.height;
       
       
}
           
    
}
