package objetos;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class ListadeTiros {
    private int numeros_de_tiros;
    ArrayList<Tiro> tiros;
    public ListadeTiros(){
        this.numeros_de_tiros = 0;
        tiros = new ArrayList();
    }
    
    protected void novo_tiro(GameContainer gc, int inclinacao, Nave nave) throws SlickException{
        
        tiros.add(new Tiro( gc,inclinacao, nave.x + nave.image.getWidth()/2, nave.y + nave.image.getHeight()/2));
    }
    
    public void update(){

        int j = 0;
        while( j < tiros.size()){
            tiros.get(j).update();
            if (tiros.get(j).fora_da_tela()) tiros.remove(j);
            else j++;
        }
            
    }
    
    public void render(GameContainer gc, Graphics g){
        for (int i = 0; i< tiros.size();i++) tiros.get(i).render( gc,  g);
    }
}
