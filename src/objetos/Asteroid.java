/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import slick.Game;

/**
 *
 * @author lucio
 */
public class Asteroid {
    
    private final String image_path = "imagens/tiro.png";
    private double x;
    private double y;
    private final Image image;
    private int angulo;
    private final int velocidade = 1; 
    
    
    
    public Asteroid(GameContainer gc, int inclinacao) throws SlickException{
       
        this.angulo = inclinacao;
        
        this.image = new Image(image_path);
        this.x = (Game.width/2) - (this.image.getWidth()/2);
        this.y = Game.height/2 - this.image.getHeight()/2;
        this.image.setCenterOfRotation(this.image.getWidth()/2,this.image.getHeight()/2);
        image.setRotation((float)Math.toRadians(angulo));
        
        
    }
    
    public void render(GameContainer gc, Graphics g){
        
        
        g.drawImage(this.image, (int)this.x, (int)this.y);

    }
    
    public void update(){
        this.move();

    }
    
    private void move(){
        System.out.println();
       this.y -= velocidade * Math.cos(Math.toRadians(angulo));
       this.x += velocidade * Math.sin(Math.toRadians(angulo));
    }
    public void finalize (){
        
    }
    public double get_Y(){
        return y;
    }

    public boolean fora_da_tela() {
        if ((this.x > Game.width) || (this.x < 0))
            return true;
        if ((this.y > Game.height) || (this.y < 0))
            return true;
        return false;
    }
    
}
