/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import org.newdawn.slick.Graphics;

/**
 *
 * @author lucio
 */
public class Botao {
    private int x;
    private int y;
    private String texto;
    public Botao(int x,int y, String texto){
        this.x = x;
        this.y = y;
        this.texto = texto;
    }
    public void render(Graphics g){
        g.drawRect(this.x, this.y, 100, 100);
        g.drawString(texto, this.x + 10, this.y);
        
    }
}
