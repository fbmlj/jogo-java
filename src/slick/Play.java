
package slick;

import objetos.*;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;


public class Play extends BasicGameState{
    
    Image playnow;
    Image exitGame;
    Nave nave;
    
    private static long delta_time;
    private long StartTime;
    
    public Play(int state){
    }
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
        this.nave = new Nave(gc);
        this.StartTime = System.currentTimeMillis();
        
        
    }
    
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{
        
        this.nave.tiros.render(gc, g);
        this.nave.render(gc, g);
    }
    
    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException{
        Input input = gc.getInput();
        calcula_delta_time();
        
        this.nave.update(input, gc);
        
        this.nave.tiros.update();
        
    }
    public int getID(){
        return 1;
    }
    private void calcula_delta_time(){
        long now_time = System.currentTimeMillis();
        this.delta_time = now_time - StartTime;
        this.StartTime = now_time;
        if (delta_time == 0 ) this.delta_time +=1;
        
    }
    public static long get_delta_time(){
        return delta_time;
    }
    public static double get_delta_time_seconds(){
        return (delta_time*0.1)/1000;
    }
            
    
}
